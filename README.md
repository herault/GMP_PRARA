# GMP_PRARA Mathilde project

***Leonard Herault & Adrien Mazuel***

## Abstract

## Availabilty of data


## Analysis and script

We provide in this repository the snakemake workflow (Snakefile.py) and its configuration (config/snakemake_workflow.yml) we developped to analyse the single cell RNA and ATAC seq data.
It is build mainly on R scripts with [Seurat v4](https://satijalab.org/seurat/) for the sample integration and the clustering of the cells. The ATAC cell sample are treated with [Signac](https://satijalab.org/signac/). It uses [pySCENIC](https://pyscenic.readthedocs.io/en/latest/) command line tools for the SCENIC part of the workflow.

See the material and method section of our manuscript for more details.


## Installation

This snakemake workflow works with conda on Linux.
You need first to download and install [conda with python version 3.7](https://docs.conda.io/en/latest/miniconda.html).
Then once you have downloaded the repository, you can create the snakemake environment with:

    conda env create -n snakemake -f config/snakemake_env.yml
    
    
The workflow starts from the output of the cellranger pipeline [v4](https://support.10xgenomics.com/single-cell-gene-expression/software/pipelines/) from 10X returning for each of our samples a filtered gene-barcode-matrix. 
Each matrix is stored in Market Exchange Format (MEX). It also contains TSV files with genes and barcode sequences corresponding to row and column indices, respectively.

You can download all these files in a tar archive on GEO under accession code GSE181190 (supplementary data section).
To run the workflow you need to create a folder input/GEO in the downloaded repository and place the tar archive inside.
You can now activate snakemake env and then launch the workflow.

### Activate snakemake environment

    conda activate snakemake

### Command line to launch the  workflow 
being in the downloaded repository:

    snakemake -j 20 \
        -nps Snakefile.py \
        --configfile config/snakemake_workflow.yml \
        --use-conda


### command lines to launch the snakemake workflow on a cluster with slurm
First being in the downloaded repository create directory where standard output and error will be written:
    
    mkdir -p cluster/snakemake/

Then launch the snakemake (you have to set the local variables to en_US.UTF-8 and add the absolute miniconda3/bin path in your $PATH if it is not the case):
### command line to launch the snakemake workflow on a cluster with slurm

    export LC_ALL=C\
    export PATH="/shared/home/lherault/bin/miniconda3/bin:$PATH"
    conda activate snakemake
    snakemake -j 14 \
        -ps Snakefile.py \
        --configfile config/snakemake_workflow.yml \
        --use-conda \
        --cluster-config config/cluster.yml \
        --cluster "sbatch -A {cluster.account} \
        -p {cluster.partition} \
        -N {cluster.N} \
        -t {cluster.time} \
        --job-name {cluster.name} \
        --cpus-per-task {cluster.cpus-per-task}\
        --output {cluster.output} \
        --error {cluster.error}" 


