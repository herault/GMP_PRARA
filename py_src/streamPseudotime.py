import sys, getopt
import pandas as pd
import stream as st
import pickle
import matplotlib.pyplot as plt
import numpy as np
import re as re
import random

random.seed(1993)

def main(argv):
  inputfile = ''
  conditionCells = ''
  conditionColors = ''
  epgAlpha = ''
  epgMu = ''
  epgLambda = ''
  outputDir = ''
  try:
    opts, args = getopt.getopt(argv, "hi:c:v:a:m:l:o:", ["inputFile=", "conditionCells=", "conditionColors", "epgAlpha=", "epgMu=", "epgLambda=", "outputDir="])
  except getopt.GetoptError:
    print('StreamAnalysis.py -i <inputfile> -c <conditionCells> -C <conditionColors> -a <epgAlpha> -m <epgMu> -l <epgLambda> -o <outputDir>')
    sys.exit(2)
  for opt, arg in opts:
    if opt == '-h':
      print('StreamAnalysis.py -i <inputfile> -c <conditionCells> -v <conditionColors> -a <epgAlpha> -m <epgMu> -l <epgLambda> -o <outputDir>')
      sys.exit()
    elif opt in ("-i", "--inputFile"):
      inputFile = arg
      print('Input file is "', inputFile)
    elif opt in ("-c", "--conditionCell"):
      conditionCells = arg
      print('conditionCells file is "', conditionCells)
    elif opt in ("-v", "--conditionColors"):
      conditionColors = arg
      print('conditionColors file is "', conditionColors)
    elif opt in ("-a", "--epgAlpha"):
      epgAlpha = float(arg)
      print('epgAlpha number "', epgAlpha)
    elif opt in ("-m", "--epgMu"):
      epgMu = float(arg)
      print('epgMu number "', epgMu)
    elif opt in ("-l", "epgLambda"):
      epgLambda = float(arg)
      print('epgLambda number "', epgLambda)
    elif opt in ("-o", "outputDir"):
      outputDir = arg
      print('Output file is "', outputDir)
      
  st.set_figure_params(dpi=80,style='white',figsize=[5.4,4.8], rc={'image.cmap': 'viridis'})
  
  adata=st.read(file_name = inputFile, workdir = outputDir)
  st.add_cell_labels(adata, file_name=conditionCells)
  st.add_cell_colors(adata, file_name=conditionColors)
  
  st.select_top_principal_components(adata, first_pc=True, n_pc=15)
  st.dimension_reduction(adata, method="mlle", feature="top_pcs", n_components=4, n_jobs=12, n_neighbors=50)
  st.seed_elastic_principal_graph(adata, n_clusters=10)
  st.elastic_principal_graph(adata=adata, epg_alpha=epgAlpha, epg_mu=epgMu, epg_lambda=epgLambda)
  st.extend_elastic_principal_graph(adata, epg_ext_mode='WeigthedCentroid', epg_ext_par=0.8)
  
  # Save plot as png
  st.plot_stream(adata, root="S1", color=['S1_pseudotime'], log_scale=True, factor_zoomin=200, save_fig=True, fig_path=outputDir, fig_format="png")
  
  adata.obs.to_csv("".join([outputDir,"stream_metadata.csv"]))
  
if __name__ == "__main__":
    main(sys.argv[1:])
      
