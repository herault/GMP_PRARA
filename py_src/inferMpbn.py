# coding: utf-8

# In[58]:

import sys, getopt, os
import bonesis
import pandas as pd
from colomoto_jupyter import tabulate
import mpbn
import itertools
import math
import numpy
import networkx as nx
import pickle


def main(argv):
    influenceGraph = ''
    outDir = ''
    try:
        opts, args = getopt.getopt(argv,"hi:o:c:",["influenceGraph=","outDir=","ncores="])
    except getopt.GetoptError:
        print('inferMpbn.py -i <influenceGraph> -o <outDir> -c <ncores>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('inferMpbn.py -i <influenceGraph> -o <outDir> -c <ncores>')
            sys.exit()
        elif opt in ("-i", "--influenceGraph"):
            influenceGraph = arg
        elif opt in ("-o", "--outDir"):
            outDir = os.getcwd() +"/"+ arg
            print('Output dir is "', outDir)
        elif opt in ("-c", "--ncores"):
            ncores = arg
            print('number of cores used is "', ncores)
    
    # Load influence graph:
    influenceGraphTable = pd.read_table(influenceGraph)  
    
    influenceGraphTable

    ## remove two interactions that avoid complete graph solutions
    ## importance score below 1
    influenceGraphTable = influenceGraphTable[[not i for i in (influenceGraphTable["target"]=="Zfpm1") & (influenceGraphTable["tf"]=="Gata2")]]
    ## self loop Bclaf1
    influenceGraphTable = influenceGraphTable[[not i for i in (influenceGraphTable["target"]=="Bclaf1") & (influenceGraphTable["tf"]=="Bclaf1")]]


    # Remove duplicated edges (bib net edges recovered with scenic)


    influenceGraphTable = influenceGraphTable.drop_duplicates(subset=['tf', 'target', 'mor'])
    inf = []
    for r in influenceGraphTable.index:
        inf.append((influenceGraphTable["tf"][r],influenceGraphTable["target"][r],dict(sign= influenceGraphTable["mor"][r])))
        dom1 = bonesis.InfluenceGraph(inf,maxclause = 3)
    #inf.append(("CIPKIP","CDK46CycD",dict(sign=
    #dom1

    len(dom1.edges)


    # Write the influence graph used

    nx.write_gpickle(dom1,outDir+"/infGraph.gpickle")


    # Inspect dual interactions


    Dual = influenceGraphTable[influenceGraphTable.duplicated(subset=['tf', 'target'], keep=False)]
    DualPos = Dual[Dual["mor"]==1]
    DualNeg = Dual[Dual["mor"]==-1]

    Dual.to_csv(outDir+"dualInteractions.csv")

    # Get all the possible combination of interactions


    a = DualPos.index
    b = DualNeg.index

    [[a[i],b[i]] for i in range(len(a))]

    inputdata = [[a[i],b[i]] for i in range(len(a))]
    result = list(itertools.product(*inputdata))


    # Get all the possible maximum influence graph


    influenceGraphTableWithoutDual = influenceGraphTable.drop_duplicates(subset=['tf', 'target'],keep=False)
    influenceGraphTableWithoutDual
    possibleMaxInfGraph = [pd.concat([influenceGraphTableWithoutDual, influenceGraphTable.loc[list(i)]]) for i in result]


    len(possibleMaxInfGraph)


   

    # Function for building constraints TODO take the constraints as an input file


    def buildConstraints(influenceGraphTable,data, maxclause = 3,exact =True):
    
    #influenceGraphTable = influenceGraphTable[[not i for i in (influenceGraphTable["target"]=="Irf1") & (influenceGraphTable["tf"]=="Stat1")]]
    #influenceGraphTable = influenceGraphTable[[not i for i in (influenceGraphTable["target"]=="Irf1") & (influenceGraphTable["tf"]=="Irf9")]]
    #influenceGraphTable = influenceGraphTable[[not i for i in (influenceGraphTable["target"]=="Stat1") & (influenceGraphTable["tf"]=="Stat1")]]
    #influenceGraphTable = influenceGraphTable[[not i for i in (influenceGraphTable["target"]=="Irf1") & (influenceGraphTable["tf"]=="Irf1")]]

    #print(len(influenceGraphTable))
        inf = []
        for r in influenceGraphTable.index:
            inf.append((influenceGraphTable["tf"][r],influenceGraphTable["target"][r],dict(sign= influenceGraphTable["mor"][r])))
    
    #print(len(inf))
    
    #inf.append(("Stat1","Myc",dict(sign=1)))

        dom1 = bonesis.InfluenceGraph(inf, maxclause = maxclause,exact=exact)
        dom1
            #print(len(dom1.edges))
        bo = bonesis.BoNesis(dom1, data)
        bo.settings["parallel"] = ncores

        fT = bo.fixed(~bo.obs("T"))
        fEr = bo.fixed(~bo.obs("Er"));
        fMk = bo.fixed(~bo.obs("Mk"));
        fMye = bo.fixed(~bo.obs("Mye"));
        #fQuiesc = bo.fixed(~bo.obs("HSC_G0"));
        #fNeu = bo.fixed(~bo.obs("Neu"));
        #fMast = bo.fixed(~bo.obs("Mast"));

        start = ~bo.obs("HSC_start")


        start >= ~bo.obs("HSC_SR")

        start >= fT;
        start >= fEr;
        start >= fMk;
        start >= fMye;
        #start >= fMast;
        #start >= fNeu;
        start >= ~bo.obs("HSC_G0");
        start >= ~bo.obs("diff");
        start >= ~bo.obs("diff2");


        ~bo.obs("diff") >= fEr
        ~bo.obs("diff") >= fMk
        ~bo.obs("diff") >= fMye

        ~bo.obs("diff2") >= fT

        #~bo.obs("diff") >= fNeu
        #~bo.obs("diff") >= fMast

        #~bo.obs("HSC_G0") >= ~bo.obs("diff")
        ~bo.obs("HSC_SR") >= ~bo.obs("HSC_G0")

        ~bo.obs("HSC_G0") >= ~bo.obs("diff2")
    
        #~bo.obs("HSC_ifn") >= ~bo.obs("HSC_G0")

        #~bo.obs("HSC_start") >= ~bo.obs("HSC_ifn")
    
        ~bo.obs("HSC_ifn") >= ~bo.obs("diff")


        ~bo.obs("diff") / ~bo.obs("HSC_G0")
        ~bo.obs("diff") / ~bo.obs("HSC_SR")
        ~bo.obs("diff") / start

        #~bo.obs("diff2") / ~bo.obs("HSC_G0")
        #~bo.obs("diff2") / ~bo.obs("HSC_SR")
        #~bo.obs("diff2") / start
        ~bo.obs('zero') / fMye
        ~bo.obs('zero') / fMk
        #~bo.obs('zero') / fEr
        ~bo.obs('zero') / fT




        #Universal reachable fixed points
        ~bo.obs("HSC_start") >> "fixpoints" ^ {bo.obs(obs) for obs in ["T", "Mye","Er","Mk"]};
        #~bo.obs("HSC_start") >> "fixpoints" ^ {bo.obs(obs) for obs in ["T", "Mye","Er","Mk","HSC_G0"]};

        #~bo.obs("HSC_G0") >> "fixpoints" ^ {bo.obs(obs) for obs in ["T", "Mye","Er","Mk"]};
    
    #Constraints on mutant

        with bo.mutant({"Junb":1}) as m:
            fQuiescMk = m.fixed(~m.obs("oldPmK"))
            #fQuiescT = m.fixed(~m.obs("mutJunbPT"))
            for cfg in bonesis.matching_configurations(m.obs("HSC_start")):
                #print(cfg)
                cfg >> "fixpoints" ^ {m.obs("oldPmK")};
                #cfg / ~m.obs("nrMut")


        return bo 


# Function to discard cyclic



    def has_cyclic(bn):
        mbn = mpbn.MPBooleanNetwork(bn)
        for a in mbn.attractors():
            if "*" in a.values():
                return True
        return False


    # data observations TO DO take it as input


    geneOrder = ["Junb","Gata2","Tal1","Bclaf1","Myc","Spi1","Ikzf1","Gata1","Klf1","Cebpa","Fli1","Zfpm1","Irf1","Irf9","Stat1","CDK46CycD","CIPKIP"]



    data = {
        "HSC_start": {"Ikzf1": 0, "Gata1": 0, "Klf1": 0,"Cebpa":0,"Fli1":1,"Spi1":0,"Junb":0,"Tal1":1,"Myc":0,"Zfpm1":0,"CDK46CycD":0,"CIPKIP":0,"Irf1":0,"Irf9":0,"Stat1":0},
        "HSC_SR": {"Ikzf1": 0, "Gata1": 0, "Klf1": 0,"Cebpa":0,"Fli1":1,"Spi1":0,"Junb":0,"Tal1":1,"Myc":0,"Zfpm1":0,"CDK46CycD":1,"CIPKIP":0,"Irf1":0,"Irf9":0,"Stat1":0},
        "HSC_G0": {"Ikzf1": 0, "Gata1": 0, "Klf1": 0,"Cebpa":0,"Fli1":1,"Spi1":0,"Junb":1,"Tal1":1,"Bclaf1":0,"Zfpm1":0,"Gata2":1,"CIPKIP":1,"Irf1":0,"Irf9":0,"Stat1":0},
        "T": {"Ikzf1": 1, "Gata1": 0, "Klf1": 0,"Cebpa":0,"Spi1":1,"Fli1":0,"Zfpm1":0,"Junb":0,"CIPKIP":0,"Irf1":0,"Irf9":0,"Stat1":0},
        "Mye": {"Ikzf1": 0,"Gata1": 0,"Klf1": 0,"Cebpa":1,"Spi1":1,"Fli1":0,"Zfpm1":0,"Tal1":0,"Junb":0,"Bclaf1":0,"CIPKIP":0,"Irf1":0,"Irf9":0,"Stat1":0},
        "Mk": {"Ikzf1": 0, "Gata1": 1, "Klf1": 0,"Cebpa":0,"Fli1":1,"Spi1":0,"Junb":0,"Tal1":1,"Zfpm1":1,"CIPKIP":0,"Irf1":0,"Irf9":0,"Stat1":0},
        "Er": {"Ikzf1": 0, "Gata1": 1, "Klf1": 1,"Cebpa":0,"Fli1":0,"Spi1":0,"Junb":0,"Tal1":1,"Zfpm1":1,"CIPKIP":0,"Irf1":0,"Irf9":0,"Stat1":0},
        "diff": {"Ikzf1": 0,"Gata1": 0, "Klf1": 0,"Cebpa":0,"Myc":1,"Spi1":1,"Junb":0,"Zfpm1":0,"Bclaf1":1,"Fli1":0,"CDK46CycD":1,"CIPKIP":0,"Irf1":0,"Irf9":0,"Stat1":0},
        "diff2": {"Ikzf1": 0,"Gata1": 0, "Klf1": 0,"Cebpa":0,"Myc":1,"Spi1":1,"Junb":0,"Zfpm1":0,"Bclaf1":1,"Fli1":0,"CDK46CycD":1,"CIPKIP":0,"Irf1":0,"Irf9":0,"Stat1":0},
        "HSC_ifn": {"Ikzf1": 0, "Gata1": 0, "Klf1": 0,"Cebpa":0,"Spi1":0,"Bclaf1":0,"Zfpm1":0,"Irf1":1,"Irf9":1,"Stat1":1},
        "zero": {n: 0 for n in dom1},
        "oldPmK":{'Zfpm1': 1,'Tal1': 1,'Stat1': 0,'Spi1': 0,'Myc': 0,'Klf1': 0, 'Junb': 1,'Irf9': 0,'Irf1': 0,'Ikzf1': 0,'Gata2': 0,'Gata1': 1,'Fli1': 1,'Cebpa': 0,'CIPKIP': 1,'CDK46CycD': 0,'Bclaf1': 0}
    }

    #"Mast": {"Ikzf1": 0,"Gata1": 0,"Klf1": 0,"Cebpa":1,"Spi1":1,"Fli1":0,"Zfpm1":0,"Tal1":0,"Junb":0,"Bclaf1":0,"Gata2":1},
    #"Neu": {"Ikzf1": 0,"Gata1": 0,"Klf1": 0,"Cebpa":1,"Spi1":1,"Fli1":0,"Zfpm1":0,"Tal1":0,"Junb":0,"Bclaf1":0,"Gata2":0},
    #"HSC_start2": {"Ikzf1": 0, "Gata1": 0, "Klf1": 0,"Cebpa":0,"Fli1":1,"Spi1":0,"Junb":0,"Tal1":1,"Myc":0,"Zfpm1":0,"Bclaf1":1,"CDK46CycD":0,"CIPKIP":0},
    #"HSC_start3": {"Ikzf1": 0, "Gata1": 0, "Klf1": 0,"Cebpa":0,"Fli1":1,"Spi1":0,"Junb":0,"Tal1":1,"Myc":0,"Zfpm1":0,"Bclaf1":0,"CIPKIP":0,"CDK46CycD":0,"Gata2":1},
 

    pd.DataFrame.from_dict(data, orient="index").fillna('')[geneOrder]
    
    pd.DataFrame.from_dict(data, orient="index").fillna('')[geneOrder].to_csv(outDir+"/obsData.csv")


    # Test all the possible maximum influence graphs


    for i in range(len(possibleMaxInfGraph)):
        bo1 = buildConstraints(influenceGraphTable = possibleMaxInfGraph[i],data =data)
        solutions = []
        for bn in bo1.boolean_networks(limit=100): # limit is optional
            #print(bn)whicc
            solutions.append(bn)
            print(len(solutions))


    # analyse one possible maximum influence graph TODO select a valid possible max inf graph


    bo1 = buildConstraints(influenceGraphTable = possibleMaxInfGraph[1],data =data,exact=True)
    solutions = []
    n=100
    while len(solutions) < 1:
        for bn in bo1.diverse_boolean_networks(limit=n): # limit is optional
            if(has_cyclic(bn)):
                #print("HAS CYCLIC ATTRACTORS, IGNORING")
                continue
            else:
                solutions.append(bn)
        n += 100
    
    print(len(solutions))


    # In[ ]:


    solutionsNumber = bo1.boolean_networks().count()
    print(solutionsNumber)
    pickle.dump(solutionsNumber, open(outDir+ "/solutionsNum.p", "wb" ),fix_imports=True,protocol=1 )


    # In[78]:


    table = pd.DataFrame(solutions)


    table.to_csv(outDir+"/solutionsTable.csv")


    nrules = table.nunique()
    table.nunique().to_csv(outDir+"/uniqueRulesNumber.csv")



    anwswerGraph = solutions[1].influence_graph()

    nx.write_gpickle(anwswerGraph,outDir+"/anwswerGraph.gpickle")
    view.standalone(output_filename=outDir+"/constraints.asp")
    pickle.dump(solutions, open(outDir+"/solutions.p", "wb" ),fix_imports=True,protocol=1 )



if __name__ == "__main__":
    main(sys.argv[1:])


