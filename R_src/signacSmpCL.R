##------------------------------------------------------------------------------
## L. Herault
##------------------------------------------------------------------------------

## -----------------------------------------------------------------------------
## Libraries
## -----------------------------------------------------------------------------
library(Signac)
library(Seurat)
library(GenomeInfoDb)
library(EnsDb.Mmusculus.v79)
library(ggplot2)
library(patchwork)
library(getopt)

set.seed(1234)

## -----------------------------------------------------------------------------
## Command line args
## -----------------------------------------------------------------------------

spec = matrix(c(
  'help',        'h', 0, "logical",   "Help about the program",
  'inputDir',  'i', 1, "character", "REQUIRED : Celranger data raw dir with filtered_peak_bc_matrix.h5,singlecell.csv and fragments.tsv.gz files",
  'outdir',     'o',1, "character", 'Outdir path (default ./)',
  "num_dim",          'n', 1, "numeric", "first n dimension of the SVD to use for the reclustering",
  "firstDimLSI",   "f",0, "logical", "Use first LSI dim, default is do not use as it is often strongly correlated to sequencing depht",
  "logfc_threshold", "d", 1, "numeric", "logfc threshold for finding cluster markers (1 cluster vs all deg) 0.25 by default",
  "sampleInfo",   "s", 1, "character", "sample information that will be added to pData with the following format: age=2_months,runDate=10_12_2017..",
  "peakRegionFragmentsMadUp", "u", 1, "numeric", "cell with peaks in region fragment upper than u mad will be discarded (default n = 2)",
  "peakRegionFragmentsMadLow", "l", 1, "numeric", "cell with peaks in region fragment lower than l mad will be discarded (default n = 2)",
  "pctReadsInPeaksLow", "r", 1, "numeric", "cells with percentage reads in peaks lower than r % will be discarded (default = 15)",
  "blackListRatio", "b", 1, "numeric", "cells with a blacklist ratio higher than b will be discarded (default b = 0.05)",
  "nucleosomeSignal", "c", 1 , "numeric", "cells with a nucleosome signal higher tban n will be discarded (default n = 4)",
  "tssEnrichment", "t", 1, "numeric","cells with a tssEnrichment lower than t will be discarded (default t = 2)"
  
), byrow=TRUE, ncol=5);



opt = getopt(spec)


# if help was asked, print a friendly message
# and exit with a non-zero error code

args <- commandArgs()
if ( !is.null(opt$help) | is.null(opt$inputDir)) {
  cat("Perform signac sample processing, quality control, genes activity, peak calling, DE peaks")
  cat(getopt(spec, usage=TRUE))
  q(status=1)
}


if (is.null(opt$logfc_threshold)) {
  opt$logfc_threshold = 0.25
} 

if (is.null(opt$num_dim)) {
  opt$num_dim = 40
} 

if (is.null(opt$firstDimLSI)) {
  opt$firstDimLSI <- FALSE
}

if (is.null(opt$peakRegionFragmentsMadUp)) {
  opt$peakRegionFragmentsMadUp <- 2
}

if (is.null(opt$peakRegionFragmentsMadLow)) {
  opt$peakRegionFragmentsMadLow <- 2
}

if (is.null(opt$pctReadsInPeaksLow)) {
  opt$pctReadsInPeaksLow <- 15
}

if (is.null(opt$blackListRatio)) {
  opt$blackListRatio <- 0.05
}

if (is.null(opt$nucleosomeSignal)) {
  opt$nucleosomeSignal <- 4
}

if (is.null(opt$tssEnrichment)) {
  opt$tssEnrichment <- 2
}


print(opt)



## ----load------------------------------------------------------------------------------------------------------
counts <- Read10X_h5(filename =paste0(opt$inputDir,"/filtered_peak_bc_matrix.h5"))
metadata <- read.csv(
  file = paste0(opt$inputDir,"/singlecell.csv"),
  header = TRUE,
  row.names = 1
)

chrom_assay <- CreateChromatinAssay(
  counts = counts,
  sep = c(":", "-"),
  genome = 'mm10',
  fragments = paste0(opt$inputDir,"/fragments.tsv.gz"),
  min.cells = 10,
  min.features = 200
)

prom <- CreateSeuratObject(
  counts = chrom_assay,
  assay = "peaks",
  meta.data = metadata
)

#Add sample infos

print(opt$sampleInfo)
sampleInfos <- strsplit(opt$sampleInfo,split=",")[[1]]
for (i in sampleInfos) {
  print(i)
  info <- strsplit(i,split = "=")[[1]][1]
  print(info)
  value <- strsplit(i,split = "=")[[1]][2]
  print(value)
  prom@meta.data[,info] <- value
}

## --------------------------------------------------------------------------------------------------------------
prom


## --------------------------------------------------------------------------------------------------------------
prom[["peaks"]]


## --------------------------------------------------------------------------------------------------------------
gr <- granges(prom)

df <- data.frame(seqnames=seqnames(gr),
                 starts=start(gr)-1,
                 ends=end(gr),
                 names=c(rep(".", length(gr))),
                 scores=c(rep(".", length(gr))),
                 strands=strand(gr))

write.table(df, file=paste0(opt$outdir,"/10X_peaks.bed"), quote=F, sep="\t", row.names=F, col.names=F)
## --------------------------------------------------------------------------------------------------------------
# extract gene annotations from EnsDb
annotations <- GetGRangesFromEnsDb(ensdb = EnsDb.Mmusculus.v79)

# change to UCSC style since the data was mapped to hg19
seqlevelsStyle(annotations) <- 'UCSC'
genome(annotations) <- "mm10"

# add the gene information to the object
Annotation(prom) <- annotations


## --------------------------------------------------------------------------------------------------------------
# compute nucleosome signal score per cell
prom <- NucleosomeSignal(object = prom)

# compute TSS enrichment score per cell
prom <- TSSEnrichment(object = prom, fast = FALSE)

# add blacklist ratio and fraction of reads in peaks
prom$pct_reads_in_peaks <- prom$peak_region_fragments / prom$passed_filters * 100
prom$blacklist_ratio <- prom$blacklist_region_fragments / prom$peak_region_fragments


## --------------------------------------------------------------------------------------------------------------
prom$high.tss <- ifelse(prom$TSS.enrichment > 2, 'High', 'Low')
png(paste0(opt$outdir,"/TSSplot.png"),units = "in",height = 6,width=5,res = 300)
TSSPlot(prom, group.by = 'high.tss') + NoLegend()
dev.off()

## --------------------------------------------------------------------------------------------------------------
prom$nucleosome_group <- ifelse(prom$nucleosome_signal > 2, 'NS > 2', 'NS < 2')
FragmentHistogram(object = prom,region = "chr1-1-100000000",group.by = 'nucleosome_group')


## ----fig.height=5,fig.width=14---------------------------------------------------------------------------------
png(paste0(opt$outdir,"/qcMetrics.png"),units = "in",height = 5,width=14,res = 300)
VlnPlot(
  object = prom,
  features = c('pct_reads_in_peaks', 'peak_region_fragments',
               'TSS.enrichment', 'blacklist_ratio', 'nucleosome_signal'),
  pt.size = 0.001,
  ncol = 5
)
dev.off()

## -------------------------------------------------------------------------------------
minCountThreshold <- median(prom$peak_region_fragments) -opt$peakRegionFragmentsMadLow*mad(prom$peak_region_fragments)
maxCountThreshold <- median(prom$peak_region_fragments)  +opt$peakRegionFragmentsMadUp*mad(prom$peak_region_fragments)
  
png(paste0(opt$outdir,"/peak_region_fragments_raw.png"),units = "in",height = 5,width=6,res = 300)
VlnPlot(
  object = prom,
  features = c('peak_region_fragments'),
  pt.size = 0.001,
) +   geom_hline(yintercept = minCountThreshold) +
  geom_hline(yintercept = maxCountThreshold)
dev.off()


## --------------------------------------------------------------------------------------------------------------
prom <- subset(
  x = prom,
  subset = peak_region_fragments > median(prom$peak_region_fragments) -opt$peakRegionFragmentsMadLow*mad(prom$peak_region_fragments) &
    peak_region_fragments < median(prom$peak_region_fragments) +opt$peakRegionFragmentsMadUp*mad(prom$peak_region_fragments) &
    pct_reads_in_peaks > opt$pctReadsInPeaksLow &
    blacklist_ratio < opt$blackListRatio &
    nucleosome_signal < opt$nucleosomeSignal &
    TSS.enrichment > opt$tssEnrichment
)
prom

## -------------------------------------------------------------------------------------

png(paste0(opt$outdir,"/peak_region_fragments_filtered.png"),units = "in",height = 5,width=6,res = 300)
VlnPlot(
  object = prom,
  features = c('peak_region_fragments'),
  pt.size = 0.001,
)
dev.off()



## ----fig.height=5,fig.width=14---------------------------------------------------------------------------------
png(paste0(opt$outdir,"/qcMetricsFiltered.png"),units = "in",height = 5,width=14,res = 300)
VlnPlot(
  object = prom,
  features = c('pct_reads_in_peaks', 'peak_region_fragments',
               'TSS.enrichment', 'blacklist_ratio', 'nucleosome_signal'),
  pt.size = 0.001,
  ncol = 5
)
dev.off()


## --------------------------------------------------------------------------------------------------------------
prom <- RunTFIDF(prom)
prom <- FindTopFeatures(prom, min.cutoff = 'q75')
prom <- RunSVD(prom, n =opt$num_dim)


## --------------------------------------------------------------------------------------------------------------
png(paste0(opt$outdir,"/DepthCor.png"),units = "in",height = 6,width=6,res = 300)
DepthCor(prom)
dev.off()

## --------------------------------------------------------------------------------------------------------------
if (opt$firstDimLSI) {
  dims = c(1:30)
} else {
  dims = c(2:30)
}

prom <- RunUMAP(object = prom, reduction = 'lsi', dims = dims)
prom <- FindNeighbors(object = prom, reduction = 'lsi', dims = dims)
prom <- FindClusters(object = prom, verbose = FALSE, algorithm = 3)
png(paste0(opt$outdir,"/UMAP_clust.png"),units = "in",height = 6,width=6,res = 300)
DimPlot(object = prom, label = TRUE) + NoLegend()
dev.off()


## --------------------------------------------------------------------------------------------------------------
gene.activities <- GeneActivity(prom)


## --------------------------------------------------------------------------------------------------------------
# add the gene activity matrix to the Seurat object as a new assay and normalize it
prom[['activities']] <- CreateAssayObject(counts = gene.activities)
prom <- NormalizeData(
  object = prom,
  assay = 'activities',
  normalization.method = 'LogNormalize',
  scale.factor = median(prom$nCount_activities)
)

## --------------------------------------------------------------------------------------------------------------
DefaultAssay(prom) <- 'activities'
markers <- FindAllMarkers(object = prom,only.pos = T)
markers <- markers[order(markers$p_val_adj),]
head(markers)
markers <- markers[markers$p_val_adj<0.05,]

write.table(markers,paste0(opt$outdir,"/geneActMarkers.tsv"),quote = F,sep = '\t',row.names = F)

## ----fig.height=8,fig.width=6----------------------------------------------------------------------------------
png(paste0(opt$outdir,"/FeaturePlot.png"),units = "in",height = 8,width=6,res = 300)
FeaturePlot(
  object = prom,
  features = head(markers$gene),
  pt.size = 0.1,
  max.cutoff = 'q95',
)
dev.off()


## --------------------------------------------------------------------------------------------------------------
DefaultAssay(prom) <- 'peaks'

da_peaks <- FindAllMarkers(
  object = prom,

  min.pct = 0.2,
  test.use = 'LR',
  latent.vars = 'peak_region_fragments'
)
da_peaks <- da_peaks[da_peaks$p_val_adj < 0.05,]
head(da_peaks)
write.table(da_peaks,paste0(opt$outdir,"/peakMarkers.tsv"),quote = F,sep = '\t',row.names = F)


## --------------------------------------------------------------------------------------------------------------

#da_peaks <- da_peaks[da_peaks$pct.1>0.4,]

closest_genes_clusterPeaks <- ClosestFeature(prom, regions = rownames(da_peaks))


head(closest_genes_clusterPeaks)


## ----fig.height=8,fig.width=8----------------------------------------------------------------------------------

plot1 <- VlnPlot(
  object = prom,
  features = rownames(da_peaks)[c(1,10)],
  pt.size = 0.1,ncol = 1,
)
plot2 <- FeaturePlot(
  object = prom,
  features =rownames(da_peaks)[c(1,10)],
  pt.size = 0.1,ncol = 1,
) 
png(paste0(opt$outdir,"/peaksPlot.png"),units = "in",height = 8,width=8,res = 300)
plot1 | plot2
dev.off()



## --------------------------------------------------------------------------------------------------------------
#closest_genes_clusterPeaks[closest_genes_clusterPeaks$query_region %in% rownames(da_peaks[order(da_peaks$avg_logFC,decreasing = T),])[c(1,10)],"gene_name"]
#To do add to peak markers table


## --------------------------------------------------------------------------------------------------------------


## --------------------------------------------------------------------------------------------------------------
# set plotting order

png(paste0(opt$outdir,"/CoveragePlot.png"),units = "in",height = 8,width=8,res = 300)
CoveragePlot(
  object = prom,
  region = rownames(da_peaks)[1],
  extend.upstream = 40000,
  extend.downstream = 20000
)
dev.off()


# # Trying macs2 call peaks
# promMacs <- prom
# 
# peaksMacs <- CallPeaks(
#   object = prom,
#   macs2.path = "/shared/home/lherault/bin/miniconda3/envs/signac_env/bin/macs2" #TO DO update env and âs macc2 path through command line
# )
# 
# 
# gr <- peaksMacs
# df <- data.frame(seqnames=seqnames(gr),
#                  starts=start(gr)-1,
#                  ends=end(gr),
#                  names=c(rep(".", length(gr))),
#                  scores=c(rep(".", length(gr))),
#                  strands=strand(gr))
# 
# write.table(df, file=paste0(opt$outdir,"/macs2_peaks.bed"), quote=F, sep="\t", row.names=F, col.names=F)
# 
# 
# 
# # call peaks for each cell type using MACS2
# DefaultAssay(pbmc) <- "cellranger"
# peaks <- CallPeaks(
#   object = pbmc,
#   group.by = "celltype",
#   additional.args = "--max-gap 50"
# )
# 
# peaks <- keepStandardChromosomes(peaks, pruning.mode = "coarse")
# 
# # remove peaks in blacklist regions
# peaks <- subsetByOverlaps(x = peaks, ranges = blacklist_hg38_unified, invert = TRUE)
# 
# # quantify peaks
# peakcounts <- FeatureMatrix(
#   fragments = Fragments(pbmc),
#   features = peaks,
#   cells = colnames(pbmc)
# )
# 
# pbmc[["ATAC"]] <- CreateChromatinAssay(
#   counts = peakcounts,
#   min.cells = 5,
#   genome = "hg38",
#   fragments = fragments,
#   annotation = annotations
# )
# 
# DefaultAssay(pbmc) <- "ATAC"
# 
# pbmc <- FindTopFeatures(pbmc, min.cutoff = 10)
# pbmc <- RunTFIDF(pbmc)
# pbmc <- RunSVD(pbmc)
# pbmc <- RunUMAP(pbmc, reduction = "lsi", dims = 2:40, reduction.name = "umap.atac")
# pbmc <- FindNeighbors(pbmc, reduction = "lsi", dims = 2:40)
# pbmc <- FindClusters(pbmc, algorithm = 3)



## --------------------------------------------------------------------------------------------------------------
saveRDS(prom,paste0(opt$outdir,"/atac.rds"))

