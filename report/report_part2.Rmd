---
title: "Promyelocites PLZF_RARA treated with Retinoic Acid"
author: "Leonard Herault"
output: html_document
---

```{r setup, include=FALSE}
suppressMessages(library(monocle))
suppressMessages(library(Seurat))
suppressMessages(library(BiocParallel))
suppressMessages(library(getopt))
suppressMessages(library(gridExtra))
suppressMessages(library(gProfileR))
suppressMessages(library(RColorBrewer))
suppressMessages(library(readxl))
suppressMessages(library(stringr))
suppressMessages(library(plyr))
suppressMessages(library(biomaRt))
suppressMessages(library(scales))

source("../R_src/getSigPerCells.R")
source("../R_src/Enrichment.R")
source("../R_src/funForSeurat.R")
source("../R_src/funForPlot.R")
source("../R_src/makeBranchedHeatmap.R")
source("../R_src/DoMultipleBarHeatmap.R")
source("../R_src/computeDiffFun.R")


dir.create("tables",showWarnings = F)
dir.create("images",showWarnings = F)
dir.create("images/png",showWarnings = F,recursive = T)


```


## Load seurat object with regulon scores

```{r cars}
seurat <-readRDS("../output/regulonAnalysis/seuratAUC.rds")
```

## cluster analysis
```{r}
Idents(seurat) <- "integrated_snn_res.0.3"
DefaultAssay(seurat) <- "AUCell"
DimPlot(seurat,label = T)
DimPlot(seurat,label = T,reduction = "tsne")
FeaturePlot(seurat,features = "nCount_RNA")
DimPlot(seurat,group.by = "sampleName")
DimPlot(seurat,group.by = "phases")
FeaturePlot(seurat,features = "percentMito")



```

## Show som signature

```{r fig.height=10,fig.width=10}
FeaturePlot(seurat,
            features = c("HSC_Chambers","Lymph_Chambers","N.er_Chambers","Mye_Chambers","Mono_Chambers","Gran_Chambers","Bcell_Chambers","NaiveT_Chambers"))

```
# Regulons table
```{r}
mainRegTable <- read.table("../output/regulonAnalysis/mainRegulonTable.tsv")
regMarker <- read.table("../output/regulonAnalysis/clusterMarkerRegulonTable.tsv")
regMarkerRA <- read.table("../output/regulonAnalysis/conditionDiffRegulonTable.tsv")

```
## DReg between condition
```{r}
DefaultAssay(seurat) <- "AUCell"
Idents(seurat) <- "condition"
markerTreatment <- FindMarkers(seurat,ident.1 = "PLZF_RARA_RA",ident.2 = "PLZF_RARA_CT",
                               logfc.threshold= 0,
                               pseudocount.use = 1,
                               min.pct = 0.1)

markerTreatment[markerTreatment$p_val_adj < 0.05,]
markerTreatment$group <- "down with RA"
markerTreatment$group[which(markerTreatment$avg_logFC > 0)] <- "up with RA"

# Compute true mean difference in score because Seurat comput only logFC
# The featureDiff functions is loaded from ../R_src/computeDiffFun.R file

markerTreatment$avg_diff <- NA
markerTreatment$regulon <- rownames(markerTreatment)

for (rm in rownames(markerTreatment)) {
  feature <- markerTreatment[rm,"regulon"]
  cells.1 <- colnames(seurat)[which(seurat$condition == "PLZF_RARA_RA")]
  cells.2 <- colnames(seurat)[which(seurat$condition == "PLZF_RARA_CT")]
  markerTreatment[rm,"avg_diff"] <- featureDiff(seurat,cells.1,cells.2,feature)
}



markerTreatment <- markerTreatment[order(markerTreatment$avg_diff),]
markerTreatment <- markerTreatment[abs(markerTreatment$avg_diff) > 0.002 & markerTreatment$p_val_adj < 0.05,]

write.table(x = markerTreatment,"tables/globalRegulonMarkers.tsv",sep = "\t",quote = F,row.names = F,col.names = T)


```

## Some regulons

## svae for scenic part of the wf
```{r}
saveRDS(seurat,"promyelocytes.rds")
```

